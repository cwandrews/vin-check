# Quick VIN

### Description
Quick VIN is an MVP for a web-app which provides basic information for any valid U.S. Vehicle VIN. A basic VIN decode is simply the minimum base for what could easily be extended to provide any and all recall information for a given year, make, and model of car or truck. Furthermore, with additional work customers could sign-up for an email list to be notified of new recalls on their vehicle and most likely well before they would otherwise be notified by the vehicle manufacturer.

### Why?
This project was created as result of the need to complete technical challenge for a job interview though departs significantly from the suggested technical challenge project, which was also to create a web app. Specifically, the general idea for the app was something that had been floating around in my head for a while now and, in my opinion, the project covers all the same coding bases as the suggested project while also providing more immediate opportunity for future improvement. Essentially, this labor of love kills two birds with one stone. 

Importantly:

*	The web frameworks of both apps use an almost indistinguishable API (Flask and Starlette).
*	Both projects revolve around a deployed and usable Python based web app using standard vanilla JQuery.
*	Although the app could have been implemented in either Flask or Starlette, an async framework was the better choice due to the reliance on calls to a third-party API. As of 2019, this would have caused a blocking issue in Flask but in Starlette it can be handled asynchronusly.
*	It is my belief that async programming is going to be more and more important moving forward, so understanding it.
*	Most importantly, Starlette is a new framework for me. As such, I believe completing a project using it has shown my ability to learn and adapt to new technologies and my interest in always learning and growing as a developer.

### Take-aways
I believe the completion of this app as a Minimum Viable Product was a great choice for the project and should provide
lots of talking points for the code review while still being very applicable to Flask. 
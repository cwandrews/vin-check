from pathlib import Path

import uvicorn as uvicorn
from starlette.applications import Starlette
from starlette.staticfiles import StaticFiles

from app.routes import NHTSAVinDecoder, SinglePageApp, NHTSARecallAPI

PACKAGE_ROOT = Path(__file__).parent
STATIC_DIR = PACKAGE_ROOT.joinpath("static")

app = Starlette()

app.mount("/static", StaticFiles(directory=STATIC_DIR), name="static")
app.add_route("/api/vin-decoder", NHTSAVinDecoder)
app.add_route("/api/recalls", NHTSARecallAPI)
app.add_route("/", SinglePageApp)


if __name__ == "__main__":
    uvicorn.run(app)

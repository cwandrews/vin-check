$(document).ready(function () {
    $("form").on("submit", function (e) {
        e.preventDefault();
        $("#VehicleData").hide();
        $("#ErrorMessage").hide();
        $("#ExecutionSpinner").show();
        $.ajax({
            type: "POST",
            url: "/api/vin-decoder",
            data: {
                vin: $("#VINInput").val()
            }
        })
            .done(function (vehicle_data) {
                if (vehicle_data) {
                    $("#VehicleYear").text(vehicle_data.ModelYear);
                    $("#VehicleMake").text(vehicle_data.Make + ' (' + vehicle_data.ManufacturerName + ')');
                    $("#VehicleModel").text(vehicle_data.Model);
                    $("#VehicleTrim").text(vehicle_data.Trim ? vehicle_data.Trim : "-");
                    $("#VehicleDoors").text(vehicle_data.Doors ? vehicle_data.Doors : "-");
                    $("#VehicleDriveType").text(vehicle_data.DriveType ? vehicle_data.DriveType : "-");
                    $("#VehicleTransmissionStyle").text(vehicle_data.TransmissionStyle ? vehicle_data.TransmissionStyle : "-");
                    $("#VehicleTransmissionSpeeds").text(vehicle_data.TransmissionSpeeds ? vehicle_data.TransmissionSpeeds : "-");
                    $("#VehicleEngineCylinderCount").text(vehicle_data.EngineNumberofCylinders ? vehicle_data.EngineNumberofCylinders : "-");
                    $("#VehicleFuelTypePrimary").text(vehicle_data.FuelTypePrimary ? vehicle_data.FuelTypePrimary : "-");

                    $("#VehicleLocationOfManufacture").text(function () {
                        if (vehicle_data.PlantCity && vehicle_data.PlantState && vehicle_data.PlantCountry) {
                            return vehicle_data.PlantCity + ', ' + vehicle_data.PlantState + ' - ' + vehicle_data.PlantCountry;
                        } else {
                            return '-'
                        }
                    });
                    $("#ExecutionSpinner").hide();
                    $("#VehicleData").show();
                } else {
                    $("#ExecutionSpinner").hide();
                    $("#ErrorMessage").show()
                }
            })
            .fail(function (xhr, status, e) {
                $("#ExecutionSpinner").hide();
                $("#ErrorMessage").show()
            });
    });
});
import re

from loguru import logger


class NHTSADataProcessor(object):
    vin_decode_fields = (
        "ErrorCode",
        "ModelYear",
        "Make",
        "ManufacturerName",
        "Model",
        "Trim",
        "Doors",
        "DriveType",
        "EngineNumberofCylinders",
        "TransmissionStyle",
        "FuelTypePrimary",
        "PlantCity",
        "PlantState",
        "PlantCountry",
        "GrossVehicleWeightRating",
        "TransmissionSpeeds",
    )
    field_cleaner_regex = re.compile(r"[\s\-]")

    def process_vin_decode(self, json: dict) -> dict:
        """
        Process the strangely formatted return from the NHTSA API into something more useful.

        :param json: the JSON data returned from the NHTSA-VPIC API.
        :return: a parsed dict with the data we want.
        """

        if json.get("Message") == "Results returned successfully":
            cleaned_data = {
                self.field_cleaner_regex.sub("", d["Variable"]): d["Value"]
                for d in json.get("Results")
                if self.field_cleaner_regex.sub("", d["Variable"])
                in self.vin_decode_fields
            }

            logger.debug(cleaned_data)
            return cleaned_data

from pathlib import Path

from aiohttp import ClientSession
from loguru import logger
from starlette.endpoints import HTTPEndpoint
from starlette.responses import UJSONResponse
from starlette.templating import Jinja2Templates

from app.util import NHTSADataProcessor

PACKAGE_ROOT = Path(__file__).parent
TEMPLATES = Jinja2Templates(directory=str(PACKAGE_ROOT.joinpath("templates")))


class SinglePageApp(HTTPEndpoint):
    async def get(self, request):
        return TEMPLATES.TemplateResponse("master.html", {"request": request})


class NHTSAVinDecoder(HTTPEndpoint):
    """
    The main, working endpoint for the app.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._data_processor = NHTSADataProcessor()

    async def post(self, request) -> UJSONResponse:
        """
        Grab the VIN from the URL query string and calls the NHTSA decoder API
        with the VIN as the argument, then filters and modifies the returned data
        before returning it to the caller.

        :param request:
        :return: UJSONResponse
        """
        base_url = "https://vpic.nhtsa.dot.gov/api/vehicles/decodevin/"
        params = {"format": "json"}

        vin = (await request.form()).get("vin").strip().upper()
        logger.debug(f"Posted VIN: {vin}")
        async with ClientSession() as s:
            async with s.get(f"{base_url}{vin}", params=params) as resp:
                raw_data = await resp.json()
                clean_data = self._data_processor.process_vin_decode(raw_data)
                if clean_data["ErrorCode"].startswith("0 - "):
                    return UJSONResponse(clean_data)
                else:
                    return UJSONResponse(dict(), status_code=500)


class NHTSARecallAPI(HTTPEndpoint):
    """
    This is more or less just a stub.
    """

    async def post(self, request) -> UJSONResponse:
        """
        Retrieve details for any and all recalls for the provided year, make, and model.

        :param request: request supplying year, make, and model #TODO How?
        :return: a list of recalls for the supplied year, make, and model as a UJSONResponse
        """
        base_url = (
            "https://one.nhtsa.gov/webapi/api/Recalls/vehicle"
            f"/modelyear/2012/make/honda/model/accord"
        )
        params = {"format": "json"}

        async with ClientSession() as s:
            async with s.get(base_url, params=params) as resp:
                raw_data = await resp.json()
                if raw_data["Message"] == "Results returned successfully":
                    return UJSONResponse(raw_data)
                else:
                    return UJSONResponse(dict(), 500)

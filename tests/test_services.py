import pytest
from starlette.testclient import TestClient

from app import app


def test_services():
    client = TestClient(app)
    response = client.get("/services/vin-decoder?vin=5J6RE48399L014483")
    assert response.status_code == 200
    assert response.text != "Hello, world!"


if __name__ == "__main__":
    pytest.main(["-v"])
